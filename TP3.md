# TP 3 - Routage, ARP, Spéléologie réseau

### 2. Mise en place du lab

#### VM client:

[user@client1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:98:27:f4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe98:27f4/64 scope link
       valid_lft forever preferred_lft forever


[user@client1 ~]$ ss -ltnp
State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      100                      127.0.0.1:25                                           *:*
LISTEN     0      128                              *:7777                                         *:*
LISTEN     0      100                          [::1]:25                                        [::]:*
LISTEN     0      128                           [::]:7777                                      [::]:*


[user@client1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3
  sources:
  services: dhcpv6-client ssh
  ports: 7777/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[user@client1 ~]$ hostname
client1.net1.tp3

[user@client1 ~]$ cat /etc/hosts
127.0.0.1  localhost
::1        localhost
10.3.1.254 router router.tp3

[user@client1 ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.776 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=0.831 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=1.26 ms
^C
--- 10.3.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 0.776/0.956/1.261/0.216 ms



#### VM server:

[user@server1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:98:27:f4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.11/24 brd 10.3.2.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe98:27f4/64 scope link
       valid_lft forever preferred_lft forever
       
       
[user@server1 ~]$ ss -ltnp
State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      100                      127.0.0.1:25                                           *:*
LISTEN     0      128                              *:7777                                         *:*
LISTEN     0      100                          [::1]:25                                        [::]:*
LISTEN     0      128                           [::]:7777                                      [::]:*


[user@server1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3
  sources:
  services: dhcpv6-client ssh
  ports: 7777/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[user@server1 ~]$ hostname
server1.net2.tp3

[user@server1 ~]$ cat /etc/hosts
10.3.2.254  router router.tp3

[user@server1 ~]$ ping 10.3.2.254
PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=0.848 ms
64 bytes from 10.3.2.254: icmp_seq=2 ttl=64 time=0.505 ms
^C
--- 10.3.2.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1010ms
rtt min/avg/max/mdev = 0.505/0.676/0.848/0.173 ms

#### VM routeur:


[user@router ~]$ ip a
1: lo: <LOOPBACK> mtu 65536 qdisc noqueue state DOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:98:27:f4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.254/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe98:27f4/64 scope link tentative dadfailed
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:e0:38:dc brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.254/24 brd 10.3.2.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee0:38dc/64 scope link
       valid_lft forever preferred_lft forever


[user@router ~]$ ss -ltnp
State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      100                      127.0.0.1:25                                           *:*
LISTEN     0      128                              *:7777                                         *:*
LISTEN     0      100                          [::1]:25                                        [::]:*
LISTEN     0      128                           [::]:7777                                      [::]:*


[user@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 7777/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:


[user@router ~]$ hostname
router.tp3


[user@router ~]$ cat /etc/hosts
10.3.1.11   client client1.net1.tp3
10.3.2.11   server server1.net2.tp3


[user@router ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.52 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=1.10 ms
^C
--- 10.3.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 1.100/1.314/1.529/0.217 ms


[user@router ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=64 time=0.742 ms
64 bytes from 10.3.2.11: icmp_seq=2 ttl=64 time=0.782 ms
^C
--- 10.3.2.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 0.742/0.762/0.782/0.020 ms


On fait un sudo sysctl -w net.ipv4.conf.all.forwarding=1 sur notre VM router, pour autoriser le routage, qui nous renvoie : net.ipv4.conf.all.forwarding=1





### 3. Comprendre le routage


|      | MAC src | MAC dst | IP src | IP dst |
|------|---------|-----|----|---|
|Dans net1|fe80::598:2293:440:510c|ff02::c|10.3.1.11|10.3.1.254
|Dans net2|e0:38:dc|98:27:f4|10.3.1.11|10.3.2.11

## II. ARP

### 1. Tables ARP

Dans chaques cas suivant on a l'adresse IP, suivi de l'adresse MAC.

##### client:
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:15 REACHABLE
Requete ARP du client
10.3.1.254 dev enp0s3 lladdr 08:00:27:98:27:f4 REACHABLE
Réponse: "je suis 10.3.1.254 et j'ai l'adresse MAC 08:00:27:98:27:f4"
le client envoie son packet au routeur, celui-ci l'envera au serveur


##### server:
10.3.2.1 dev enp0s3 lladdr 0a:00:27:00:00:18 REACHABLE
Requete ARP du server
10.3.2.254 dev enp0s3 lladdr 08:00:27:e0:38:dc STALE
Réponse: "je suis 10.3.2.254 et j'ai l'adresse MAC 08:00:27:e0:38:dc"
le server va recevoir le packet


##### router:
10.3.1.11 dev enp0s3 lladdr 08:00:27:98:27:f4 STALE
Le routeur reçoit le packet du client.
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:15 REACHABLE
Il le donne à 10.3.2.
10.3.2.1 dev enp0s8 lladdr 0a:00:27:00:00:18 REACHABLE
Il envoie une requête ARP
10.3.2.11 dev enp0s8 lladdr 08:00:27:98:27:f4 STALE
Réponse: "je suis 10.3.2.11 et j'ai l'adresse MAC 08:00:27:98:27:f4"
le server va recevoir le packet

### 2. Requêtes ARP

#### A. Table ARP 1
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:15 REACHABLE
10.3.1.254 dev enp0s3 lladdr 08:00:27:98:27:f4 STALE
Cette fois on a STALE pour le 10.3.1.254 elle n'expirera pas


#### B. Table ARP 2
10.3.2.1 dev enp0s3 lladdr 0a:00:27:00:00:18 REACHABLE
10.3.2.254 dev enp0s3 lladdr 08:00:27:e0:38:dc REACHABLE
Cette fois on a REACHABLE pour le 10.3.2.254 elle expirera au bout d'un moment.


#### C. tcpdump 1
![](https://i.imgur.com/rzuZYk0.png)

#### D. tcpdump 2
![](https://i.imgur.com/QsKbeYi.png)

#### E. u okay bro ?

1. Le client envoie un packet au routeur mais qui ne lui est pas destiné.
2. Le routeur regarde dans sa table de routage pour voir s'il peut r'envoyer le packet.
3. Le routeur l'envoie au serveur.
4. Le serveur reçoit le packet du client.


### Entracte : Donner un accès internet aux VMs

On fait un "sudo ifup enp0s9 et on remet:
ONBOOT=yes dans le fichier /etc/sysconfig/network-scripts/ifcfg-enp0s9



[user@router ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[user@router ~]$ sudo firewall-cmd --reload
success

On ajoute GATEWAY=10.3.1.254 dans /etc/sysconfig/network


On test de ping 8.8.8.8:
[user@client1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=51 time=22.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=51 time=20.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=51 time=21.5 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 20.608/21.464/22.233/0.676 ms

# III. Plus de tcpdump


#### B. Analyse de trames

##### TCP :

![](https://i.imgur.com/KlBUodV.png)

Ici le client envoie son packet au server 
et le server confirme l'arrivé du packet

![](https://i.imgur.com/r3IPBqE.png)

Ici le server envoie son packet au client
et le client confirme l'arrivé du packet

##### UDP :

![](https://i.imgur.com/1vBbFQp.png)

On voit clairement que dans une connexion UDP le reçeveur du packet ne confirme pas son arrivé.

### 2. SSH

On effectue le connexion ssh:
[user@client1 ~]$ ssh user@10.3.2.11 -p 7777
Last login: Mon Mar 16 19:09:40 2020 from router

Grâce à la connexion tcpdump on voit que la connexion ssh utilise TCP.

![](https://i.imgur.com/FmswzS0.png)

Le TCP est orienté "connexion".