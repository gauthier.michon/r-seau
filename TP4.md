# TP 4 - Cisco, Routage, DHCP

## 2. Mise en place

### B. Définition d'IPs statiques

#### admin1 : 
On change l'adresse ip de enp0s3 avec la commande:
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
![](https://i.imgur.com/JcW31U4.png)

Puis on change son nom de domaine avec la commande hostname
![](https://i.imgur.com/6CwvlBa.png)


#### router1 : 

Tout d'abord pour changer l'ip du router dans le terminale depuis GNS3, on doit faire un conf t, pour se mettre en mode config
Ensuite on rentre la commande : interface fastethernet0/0 ,pour configurer l'ip de fastethernet0/0
Pour changer l'ip on rentre ensuite la commande : ip address 10.4.1.254 255.255.255.0
Et enfin on fait un : no shut , pour activer l'interface.
On fait la même chose pour fastethernet1/0
![](https://i.imgur.com/6Esno69.png)
Et enfin on rentre la commande copy running-config startup-config, pour que nos modifications soient permanentes.


On modifie ensuite le nom du routeur depuis l'interface de GNS 3, pour cela le routeur doit être éteint.
![](https://i.imgur.com/7zXE07O.png)

#### guest1 : 

La config du guest1 est beaucoup plus simple. Il suffit de rentrer la commande ip 10.4.2.11.
![](https://i.imgur.com/9h9izvp.png)

Et on fait un save, pour sauvegarder les modifictions.
![](https://i.imgur.com/7yxu0ui.png)

On modifie ensuite le nom du guest depuis l'interface de GNS 3.

#### Vérification : 

guest peut joindre le routeur:
![](https://i.imgur.com/VzPJ0mC.png)

admin peut joindre le routeur:
![](https://i.imgur.com/YzyRfAJ.png)

Le routeur peut joindre guest:
![](https://i.imgur.com/3Vt5xmk.png)

Le routeur peut joindre admin:
![](https://i.imgur.com/j2dTXlT.png)

Pour vérifier la table arp du routeur, on fait un : show arp.
![](https://i.imgur.com/xA23UEa.png)
On voit que l'ip de l'admin et du guest sont bien présents.

On va maintenant vérifier que les adresses mac sont bonnes.
Pour l'admin dont l'ip est 10.4.1.11: 
![](https://i.imgur.com/ENq9Uwi.png)
L'adresse MAC est bien 08:00:27:98:27:f4, comme dans la table ARP du routeur

Pour le guest dont l'ip est 10.4.2.11: 
![](https://i.imgur.com/D0KT0pN.png)
L'adresse MAC est bien 00:50:79:66:68:00, comme dans la table ARP du routeur

### C. Routage : 

#### admin1 : 

Pour ajouter une router d'admin jusqu'à guest, on fait un : 
sudo nano /etc/sysconfig/network-scripts/route-enp0s3.
Dedans on rentre : 10.4.2.11/24 via 10.4.1.254 dev enp0s3.
On fait un systemctl restart network.
Puis un ip route show pour vérifier que notre route est bien ajouter.
![](https://i.imgur.com/5YpEoYz.png)


#### guest1 : 

Pour guest1 il suffit de mettre une gateway avec l'adresse du routeur.
![](https://i.imgur.com/YpX1fdX.png)


#### Vérification : 

On vérifie que admin1 peut joindre guest1 en passant par le routeur.
Pour cela on utilise la commande traceroute:
![](https://i.imgur.com/dYNYuiy.png)
Le ping s'effectue correctement et passe bien par le routeur.


On vérifie que guest1 peut joindre admin1 en passant par le routeur.
Pour cela on utilise la commande trace:
![](https://i.imgur.com/0xzVszJ.png)
Le ping s'effectue correctement et passe bien par le routeur.


## II. Topologie 2 : dumb switches : 

### 2. Mise en place : 

### C. Vérification : 

On veut vérifier que guest1 peut toujours joindre le admin1 et réciproquement et que les paquets transitent toujours par router.

Pour cela on fait les mêmes commandes que juste avant.
![](https://i.imgur.com/gqzuFuN.png)
L'admin peut toujours joindre le guest en passant par le routeur.

![](https://i.imgur.com/QYvMeq4.png)
Le guest peut toujours joindre l'admin en passant par le routeur.


## III. Topologie 3 : adding nodes and NAT : 

### B. VPCS : 

#### guest2 : 
On rentre l'adresse ip voulu.
![](https://i.imgur.com/0J2heAQ.png)

Puis on ajoute une route dans admin pour que les deux puissent se joindre.
![](https://i.imgur.com/ADHDhIw.png)

On vérifie maintenant que le guest2 peut joindre l'admin, avec un ping :
![](https://i.imgur.com/BMeVLf4.png)
Le guest 2 peut joindre l'admin.


#### guest3 : 
On rentre l'adresse ip voulu.
![](https://i.imgur.com/Cvb7e3d.png)

Puis on ajoute une route dans admin pour que les deux puissent se joindre
![](https://i.imgur.com/sveBxek.png)

On vérifie maintenant que le guest3 peut joindre l'admin, avec un ping :
![](https://i.imgur.com/RLnlcaA.png)
Le guest 3 peut joindre l'admin.

### C. Accès WAN : 

#### Donner un accès WAN au routeur : 

Tout d'abord il faut rajouer un slot d'adaptateur pour relier le NAT au routeur.
![](https://i.imgur.com/mveqn3e.png)

Ensuite pour changer l'ip du router dans le terminale depuis GNS3, on doit faire un conf t, pour se mettre en mode config
Ensuite on rentre la commande : interface fastethernet2/0 ,pour configurer l'ip de fastethernet2/0
Pour changer l'ip on rentre ensuite la commande : ip address dhcp
Et enfin on fait un : no shut , pour activer l'interface.
On vérifie avec : show ip int br
![](https://i.imgur.com/oQZnrWN.png)
On a bien récupérer une IP en DHCP sur l'interface branchée au nuage NAT.

On repère ensuite l'interface qui pointe vers le WAN (ici le fastethernet2/0) et les interfaces qui pointent vers les LAN's (ici fastethernet0/0 et fastethernet1/0)

On fait donc un : ip nat outside pour fastethernet2/0
et un : ip nat inside pour les 2 autres

Ensuite on fait un : access-list 1 permit any

Puis on configure le nat avec : 
ip nat inside source list 1 interface fastEthernet2/0 overload


#### Configurer les clients : 

On commance par ajouter une route par défaut aux clients:

pour le routeur, on va dans /etc/sysconfig/network et on rentre GATEWAY=10.4.1.254

On va configurer l'utilisation d'un serveur dns avec : 
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
Dedans on rajoute DNS1=1.1.1.1


#### Vérification : 

On vérifie que le routeur a bien internet : 
![](https://i.imgur.com/pgs0BUx.png)
Le ping s'effectue donc le routeur a bien internet.

On ping 8.8.8.8 avec l'admin : 
![](https://i.imgur.com/V5qf8HM.png)
Le ping s'effectue donc l'admin a bien internet.

On ping 8.8.8.8 avec le guest1 : 
![](https://i.imgur.com/96oo0fD.png)
Le ping s'effectue donc le guest1 a bien internet.

On ping 8.8.8.8 avec le guest2 : 
![](https://i.imgur.com/95fSL4d.png)
Le ping s'effectue donc le guest2 a bien internet.

On ping 8.8.8.8 avec le guest3 : 
![](https://i.imgur.com/oFSxGOZ.png)
Le ping s'effectue donc le guest3 a bien internet.


## IV. Topologie 4 : home-made DHCP : 

On vérifie que l'on a des ports UDP en écoute:
![](https://i.imgur.com/6P6qmJS.png)
Le port 67, qui est un port dhcp est en écoute

On change l'ip de guest2 avec la commande : ip dhcp
![](https://i.imgur.com/X0y9ATd.png)
ça nous donne bien une ip en 10.4.2
En plus on récupère l'ip du dhcp server

Les échanges dhcp avec wireshark:
![](https://i.imgur.com/zYDUQao.png)
